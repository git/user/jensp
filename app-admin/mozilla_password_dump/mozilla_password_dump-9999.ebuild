# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit git-2 eutils

DESCRIPTION="Dump Firefox, SeaMonkey and Thunderbird passwords from profile"
HOMEPAGE="https://github.com/kholia/mozilla_password_dump"
SRC_URI=""
EGIT_REPO_URI="git://github.com/kholia/mozilla_password_dump.git"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="dev-libs/nspr
		dev-libs/nss
		dev-libs/iniparser
		dev-db/sqlite:3"
RDEPEND="${DEPEND}"

src_compile() {
	$(tc-getCC) \
	${CFLAGS} \
	-I/usr/include/nss -I/usr/include/nspr -c mozilla_password_dump.c || die
	$(tc-getCC) \
	${LDFLAGS} \
	-o mozilla_password_dump mozilla_password_dump.o \
	-lnspr4 -lnss3 -lsqlite3 -liniparser || die
}

src_install() {
	dobin ${PN}
	dodoc README
}
