# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

inherit distutils git

DESCRIPTION="A python library for writing kickstart files"
HOMEPAGE="http://fedoraproject.org/wiki/Pykickstart"
SRC_URI=""
EGIT_REPO_URI="git://git.fedorahosted.org/git/pykickstart.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
