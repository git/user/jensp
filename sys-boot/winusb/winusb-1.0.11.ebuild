# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

DESCRIPTION="Simple tool to create a usb stick installer for Windows (Vista and above)"
HOMEPAGE="http://en.congelli.eu/prog_info_winusb.html"
SRC_URI="http://en.congelli.eu/directdl/${PN}/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="x11-libs/wxGTK"
RDEPEND="${DEPEND}"
