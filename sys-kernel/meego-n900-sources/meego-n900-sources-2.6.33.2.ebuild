# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

K_PREPATCHED="yes"
UNIPATCH_STRICTORDER="yes"
K_SECURITY_UNSUPPORTED="1"

ETYPE="sources"

inherit kernel-2
detect_version

UNIPATCH_LIST="${DISTDIR}/meego-n900.patch.bz2
${FILESDIR}/mac80211.compat08082009.wl_frag+ack_v1.patch
${FILESDIR}/4013-runtime-enable-disable-of-mac80211-packet-injection.patch" 

DESCRIPTION=""
HOMEPAGE=""
SRC_URI="${KERNEL_URI} http://chaox.net/~jens/meego-n900.patch.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~arm"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
